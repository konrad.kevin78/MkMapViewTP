import Foundation
import CoreData

public protocol StoreProvider: class {
    var stores: [Store] { get }
    var context: NSManagedObjectContext! { get }
    func create(values: Dictionary<String, String>) -> Bool
    func read() -> [Store]
    func update(store: Store, values: Dictionary<String, String>) -> Bool
    func delete(store: Store) -> Bool
}
