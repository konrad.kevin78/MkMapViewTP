import UIKit

class StoresList: AppBase {
    @IBOutlet weak var storeCollectionView: UICollectionView!
    weak var storeProvider: StoreProvider?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.storeCollectionView.delegate = self
        self.storeCollectionView.dataSource = self        
        self.storeCollectionView.register(UINib(nibName: "StoresListCell", bundle: nil), forCellWithReuseIdentifier: "Store")
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.storeCollectionView.reloadData()
    }
}

extension StoresList: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storeDetailsViewController = StoresNew()
        storeDetailsViewController.storeProvider = self.storeProvider
        storeDetailsViewController.store = self.storeProvider?.stores[indexPath.row]
        self.present(PortraitNavigation(rootViewController: storeDetailsViewController), animated: true)
    }
}

extension StoresList: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.storeProvider?.stores.count ?? 0;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let store = self.storeProvider?.stores[indexPath.item] else {
            fatalError("Not possible")
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Store", for: indexPath)
        if let storeCell = cell as? StoresListCell {
            storeCell.titleLabel.text = store.name
            storeCell.latLabel.text = String(store.latitude)
            storeCell.lonLabel.text = String(store.longitude)
        }
        
        return cell
    }
}

extension StoresList: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = collectionView.bounds.width
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            width -= layout.minimumInteritemSpacing
        }
        
        return CGSize(width: width / 2, height: width / 2)
    }
    
    
}
