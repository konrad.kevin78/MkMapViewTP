import UIKit

class StoresListCell: UICollectionViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var lonLabel: UILabel!
    
    override func awakeFromNib() {
        self.contentView.layer.cornerRadius = 10
        self.contentView.backgroundColor = UIColor.lightGray
    }
}
