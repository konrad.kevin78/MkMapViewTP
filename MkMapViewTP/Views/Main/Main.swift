import UIKit
import CoreData

class Main: AppBase, StoreProvider {
    var context : NSManagedObjectContext!
    var stores: [Store] = []

    @IBOutlet weak var childContentView: UIView!
    
    lazy var mapViewController: StoresMap = {
        let mapViewController =  StoresMap()
        mapViewController.storeProvider = self
        return mapViewController
    }()
    
    lazy var listViewController: StoresList = {
        let listViewController = StoresList()
        listViewController.storeProvider = self
        return listViewController
    }()
    
    public var visibleViewController: UIViewController {
        if self.mapViewController.view.window != nil {
            return self.mapViewController
        }
        
        return self.listViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.stores = self.read()
        self.addChildViewController(self.mapViewController, in: childContentView)
    }
    
    @IBAction func switchController(_ sender: Any) {
        let visibleViewController = self.visibleViewController
        self.removeVisibleChildViewController(visibleViewController)
        if visibleViewController == self.mapViewController {
            UIView.beginAnimations("flip_animation", context: nil)
            UIView.setAnimationTransition(.flipFromRight, for: self.childContentView, cache: false)
            UIView.setAnimationDuration(0.5)
            self.addChildViewController(self.listViewController, in: childContentView)
        } else {
            UIView.beginAnimations("flip_animation", context: nil)
            UIView.setAnimationTransition(.flipFromLeft, for: self.childContentView, cache: false)
            UIView.setAnimationDuration(0.5)
            self.addChildViewController(self.mapViewController, in: childContentView)
        }
        
        UIView.commitAnimations()
    }
    
    @IBAction func touchNewStore(_ sender: Any) {
        let storeNewViewController = StoresNew()
        storeNewViewController.storeProvider = self
        self.present(PortraitNavigation(rootViewController: storeNewViewController), animated: true)
    }
    
    func create(values: Dictionary<String, String>) -> Bool {
        guard
            let name = values["name"],
            name.count > 0,
            let latString = values["latitude"],
            let latitude = Double(latString),
            let lngString = values["longitude"],
            let longitude = Double(lngString) else {
            return false
        }
        
        let store = Store(context: context)
        store.name = name
        store.latitude = latitude
        store.longitude = longitude
        self.stores.append(store)
        return self.save()
    }
    
    func read() -> [Store] {
        let request: NSFetchRequest = Store.fetchRequest()
        guard let result = try? self.context.fetch(request) else {
            return []
        }
        
        return result
    }
    
    func update(store: Store, values: Dictionary<String, String>) -> Bool {
        guard
            let name = values["name"],
            name.count > 0,
            let latString = values["latitude"],
            let lat = Double(latString),
            let lngString = values["longitude"],
            let lng = Double(lngString) else {
                return false
        }
        
        store.name = name
        store.latitude = lat
        store.longitude = lng
        return self.save()
    }
    
    func delete(store: Store) -> Bool {
        guard let index = self.stores.index(of: store), index >= 0 else {
            return false
        }
        
        self.stores.remove(at: index)
        self.context.delete(store)
        return self.save()
    }
    
    func save() -> Bool {
        do {
            try self.context.save()
        } catch {
            return false
        }
        
        return true
    }
}
