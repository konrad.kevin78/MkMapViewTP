import UIKit
import CoreLocation
import CoreData

public class StoresNew: AppBase {
    var storeProvider: StoreProvider?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var latitudeTextField: UITextField!
    @IBOutlet weak var latitudeLabel: UILabel!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var lonLabel: UILabel!
    @IBOutlet weak var lonTextField: UITextField!
    @IBOutlet weak var deleteButton: UIButton!
    
    public var store: Store?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("controllers.new_apple_store.title", comment: "")
        self.titleLabel.text = NSLocalizedString("controllers.new_apple_store.title_label", comment: "")
        self.latitudeLabel.text = NSLocalizedString("controllers.new_apple_store.title_textFieldLat", comment: "")
        self.lonLabel.text = NSLocalizedString("controllers.new_apple_store.title_textFieldLng", comment: "")
        self.titleTextField.delegate = self
        self.latitudeTextField.delegate = self
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(closeViewController))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(submitAppleStore))
        
        if store != nil {
            guard let store = store else {
                return
            }
            
            self.titleTextField.text = store.name
            self.latitudeTextField.text = String(format: "%f", store.latitude)
            self.lonTextField.text = String(format: "%f", store.longitude)
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(editAppleStore))
            self.deleteButton.isHidden = false
        }
    }

    public override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBAction func buttonDeleteHandler(_ sender: Any) {
        guard
            let provider = self.storeProvider,
            let store = self.store else {
            return
        }
        
        let valid: Bool = provider.delete(store: store)
        
        if !valid {
            let alert = UIAlertController(
                title:NSLocalizedString("app.vocabulary.error_title", comment: ""),
                message: NSLocalizedString("app.vocabulary.error_form_message", comment: ""),
                preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("app.vocabulary.close", comment: ""), style: .cancel))
            self.present(alert, animated: true)
            return
        }
        
        self.closeViewController()
        
    }
    
    @objc public func closeViewController() {
        self.dismiss(animated: true)
    }
    
    @objc public func editAppleStore() {
        guard let store = self.store, let provider = self.storeProvider else {
            return
        }
        
        var values = [String: String]()
        values["name"] = self.titleTextField.text
        values["latitude"] = self.latitudeTextField.text
        values["longitude"] = self.lonTextField.text
        
        let valid: Bool = provider.update(store: store, values: values)
        
        if !valid {
            let alert = UIAlertController(
                title:NSLocalizedString("app.vocabulary.error_title", comment: ""),
                message: NSLocalizedString("app.vocabulary.error_form_message", comment: ""),
                preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("app.vocabulary.close", comment: ""), style: .cancel))
            self.present(alert, animated: true)
            return
        }
        
        self.closeViewController()
    }
    
    @objc public func submitAppleStore() {
        var values = [String: String]()
        values["name"] = self.titleTextField.text
        values["latitude"] = self.latitudeTextField.text
        values["longitude"] = self.lonTextField.text
        
        guard let provider = self.storeProvider else {
            return
        }
        
        let valid: Bool = provider.create(values: values)
        
        if !valid {
            let alert = UIAlertController(
                title:NSLocalizedString("app.vocabulary.error_title", comment: ""),
                message: NSLocalizedString("app.vocabulary.error_form_message", comment: ""),
                preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: NSLocalizedString("app.vocabulary.close", comment: ""), style: .cancel))
            self.present(alert, animated: true)
            return
        }
        
        self.closeViewController()
    }
}

extension StoresNew: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
