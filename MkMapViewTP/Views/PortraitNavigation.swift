import Foundation
import UIKit

public class PortraitNavigation: UINavigationController {
    public override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return self.visibleViewController?.supportedInterfaceOrientations ?? super.supportedInterfaceOrientations
    }
}
