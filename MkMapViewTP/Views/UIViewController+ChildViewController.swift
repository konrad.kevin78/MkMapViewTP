import UIKit

extension UIViewController {
    func removeVisibleChildViewController(_ childController: UIViewController) {
        childController.removeFromParentViewController()
        childController.view.removeFromSuperview()
    }
    
    func addChildViewController(_ childController: UIViewController, in subview: UIView) {
        guard let view = childController.view else {
            return
        }
        
        view.frame = subview.bounds
        view.autoresizingMask = UIViewAutoresizing(rawValue: 0b111111)
        subview.addSubview(view)
        self.addChildViewController(childController)
    }
}
