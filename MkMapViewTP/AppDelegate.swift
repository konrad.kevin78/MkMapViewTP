import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var context: NSManagedObjectContext?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.context = self.loadCoreData()
        let window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoreViewController = Main()
        mainStoreViewController.context = self.context
        window.rootViewController = mainStoreViewController
        window.makeKeyAndVisible()
        self.window = window
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) { }

    func applicationDidEnterBackground(_ application: UIApplication) { }

    func applicationWillEnterForeground(_ application: UIApplication) { }

    func applicationDidBecomeActive(_ application: UIApplication) { }

    func applicationWillTerminate(_ application: UIApplication) { }
}

extension AppDelegate {
    func loadCoreData() -> NSManagedObjectContext? {
        guard
            let schemaURL = Bundle.main.url(forResource: "Store", withExtension: "momd"),
            let model = NSManagedObjectModel(contentsOf: schemaURL) else {
                return nil
        }
        
        let store = NSPersistentStoreCoordinator(managedObjectModel: model)
        guard let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        let storageURL = documentDirectory.appendingPathComponent("database.sqlite")
        print(storageURL)
        _ = try? store.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storageURL, options: nil)
        let context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.persistentStoreCoordinator = store
        self.context = context
        return context
    }
}
