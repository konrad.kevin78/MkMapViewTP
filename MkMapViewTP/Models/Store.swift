import Foundation
import CoreLocation
import MapKit

extension Store {
    public var annotation: MKAnnotation {
        let annotation = MKPointAnnotation()
        annotation.title = self.name
        annotation.coordinate = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
        return annotation
    }
}
